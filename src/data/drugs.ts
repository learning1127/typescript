export interface Drug {
  id: number;
  drug_company: string;
  diagnosis_code: string;
  email: string;
  drug_name: string;
}

export const drugs: Array<Drug> = [
  {
    id: 1,
    drug_company: "ALK-Abello, Inc.",
    diagnosis_code: "H2649",
    email: "fsommer0@dell.com",
    drug_name: "ACREMONIUM STRICTUM",
  },
  {
    id: 2,
    drug_company: "Major Pharmaceuticals",
    diagnosis_code: "M0521",
    email: "tflukes1@moonfruit.com",
    drug_name: "benzocaine",
  },
  {
    id: 3,
    drug_company: "Medline Industries, Inc.",
    diagnosis_code: "H25091",
    email: "rcalbaithe2@cafepress.com",
    drug_name: "Povidone-Iodine",
  },
  {
    id: 4,
    drug_company: "Preferred Pharmaceuticals, Inc",
    diagnosis_code: "Q399",
    email: "bcoaster3@typepad.com",
    drug_name: "Betamethasone Dipropionate",
  },
  {
    id: 5,
    drug_company: "Cardinal Health",
    diagnosis_code: "T361X2A",
    email: "gwarden4@homestead.com",
    drug_name: "Acetylcysteine",
  },
  {
    id: 6,
    drug_company: "ALK-Abello, Inc.",
    diagnosis_code: "S02110",
    email: "ponele5@parallels.com",
    drug_name: "Ragweed Western",
  },
  {
    id: 7,
    drug_company: "McKesson",
    diagnosis_code: "G90529",
    email: "sstubbings6@google.pl",
    drug_name: "Pyrithione Zinc",
  },
  {
    id: 8,
    drug_company: "Dharma Research, Inc.",
    diagnosis_code: "M1A3390",
    email: "dsellwood7@dion.ne.jp",
    drug_name: "Sodium Fluoride",
  },
  {
    id: 9,
    drug_company: "American Sales Company",
    diagnosis_code: "O691XX5",
    email: "sdanks8@g.co",
    drug_name: "diphenhydramine citrate and ibuprofen",
  },
  {
    id: 10,
    drug_company: "Lake Erie Medical DBA Quality Care Products LLC",
    diagnosis_code: "E09353",
    email: "afinden9@psu.edu",
    drug_name: "Metoprolol Tartrate",
  },
  {
    id: 11,
    drug_company: "X-GEN Pharmaceuticals, Inc.",
    diagnosis_code: "S42032A",
    email: "fhalpena@state.tx.us",
    drug_name: "fomepizole",
  },
  {
    id: 12,
    drug_company: "HANLIM PHARM. CO., LTD.",
    diagnosis_code: "K033",
    email: "btuffellb@diigo.com",
    drug_name: "DEXTRAN 70",
  },
  {
    id: 13,
    drug_company: "Allergy Laboratories, Inc.",
    diagnosis_code: "O296",
    email: "hrulfc@yale.edu",
    drug_name: "chaetomium globosum",
  },
  {
    id: 14,
    drug_company: "Sam's West Inc",
    diagnosis_code: "T485X1D",
    email: "shanrahand@umn.edu",
    drug_name: "Acetaminophen, Dextromethorphan HBr, Doxylamine succinate",
  },
  {
    id: 15,
    drug_company: "Supervalu Inc",
    diagnosis_code: "T465X3D",
    email: "adavinete@smugmug.com",
    drug_name: "Mineral oil, petrolatum, phenylephrine hcl",
  },
  {
    id: 16,
    drug_company: "REMEDYREPACK INC.",
    diagnosis_code: "S3096XS",
    email: "dyausf@smh.com.au",
    drug_name: "Atenolol",
  },
  {
    id: 17,
    drug_company: "STAT RX USA LLC",
    diagnosis_code: "T1592XS",
    email: "vbarrieg@bing.com",
    drug_name: "PIROXICAM",
  },
  {
    id: 18,
    drug_company: "Family Dollar Services Inc",
    diagnosis_code: "S06816D",
    email: "lunthankh@weather.com",
    drug_name: "Omeprazole",
  },
  {
    id: 19,
    drug_company: "Deseret Biologicals, Inc.",
    diagnosis_code: "S52392D",
    email: "tyuryatini@live.com",
    drug_name:
      "Viscum Album, Cobaltum Gluconicum, Cuprum Sulphuricum, Ferrous Fumaricum, Kali Asparaginicum, Magnesia Asparaginicum, Manganum Gluconicum",
  },
  {
    id: 20,
    drug_company: "Nelco Laboratories, Inc.",
    diagnosis_code: "S0301XS",
    email: "zbeechingj@bloglines.com",
    drug_name: "Alternaria alternata",
  },
  {
    id: 21,
    drug_company: "Ventura International LTD",
    diagnosis_code: "D598",
    email: "aelsburyk@typepad.com",
    drug_name: "Octinoxate and Titanium Dioxide",
  },
  {
    id: 22,
    drug_company: "Legacy Pharmaceutical Packaging",
    diagnosis_code: "I82819",
    email: "bheidenl@apache.org",
    drug_name: "Losartan Potassium",
  },
  {
    id: 23,
    drug_company: "DIRECT RX",
    diagnosis_code: "T51",
    email: "aspeedym@nps.gov",
    drug_name: "TRAMADOL HYDROCHLORIDE",
  },
  {
    id: 24,
    drug_company: "Mylan Pharmaceuticals Inc.",
    diagnosis_code: "Y35312D",
    email: "jmcgennn@drupal.org",
    drug_name: "nifedipine",
  },
  {
    id: 25,
    drug_company: "Ventura International LTD",
    diagnosis_code: "V9340XD",
    email: "ljakoubeco@timesonline.co.uk",
    drug_name: "Octinoxate and Oxybenzone",
  },
  {
    id: 26,
    drug_company: "LC Industries",
    diagnosis_code: "S75209",
    email: "hmaberleyp@webeden.co.uk",
    drug_name:
      "Avena Sativa Flowering Top, Capsicum, Strychnos Nux-Vomica Seed, Veratrum Album Root, and Zinc",
  },
  {
    id: 27,
    drug_company: "The Mentholatum Company",
    diagnosis_code: "V1910XD",
    email: "jortsq@ow.ly",
    drug_name: "Zinc oxide",
  },
  {
    id: 28,
    drug_company: "Nelco Laboratories, Inc.",
    diagnosis_code: "J84112",
    email: "lossennar@csmonitor.com",
    drug_name: "Peanut",
  },
  {
    id: 29,
    drug_company: "Carilion Materials Management",
    diagnosis_code: "V160XXS",
    email: "cmargerisons@cbslocal.com",
    drug_name: "diltiazem hydrochloride",
  },
  {
    id: 30,
    drug_company: "Camber Pharmaceuticals",
    diagnosis_code: "T2142",
    email: "smquharget@google.com.br",
    drug_name: "Warfarin Sodium",
  },
  {
    id: 31,
    drug_company: "General Cryogenic Technologies LLC",
    diagnosis_code: "M86122",
    email: "baldineu@indiatimes.com",
    drug_name: "Nitrogen",
  },
  {
    id: 32,
    drug_company: "HOMEOLAB USA INC.",
    diagnosis_code: "M1A3790",
    email: "hectorv@virginia.edu",
    drug_name: "ALTHAEA OFFICINALIS",
  },
  {
    id: 33,
    drug_company: "Aton Pharma, Inc.",
    diagnosis_code: "S68115S",
    email: "rwestmerlandw@utexas.edu",
    drug_name: "metyrosine",
  },
  {
    id: 34,
    drug_company: "Clinical Solutions Wholesale",
    diagnosis_code: "I7101",
    email: "jbachurax@dyndns.org",
    drug_name: "Gabapentin",
  },
  {
    id: 35,
    drug_company: "AMERICAN SALES COMPANY",
    diagnosis_code: "W5512",
    email: "mhearnsy@sohu.com",
    drug_name: "TRICLOSAN",
  },
  {
    id: 36,
    drug_company: "Bryant Ranch Prepack",
    diagnosis_code: "V660XXD",
    email: "senriquesz@epa.gov",
    drug_name: "Hydroxyzine Pamoate",
  },
  {
    id: 37,
    drug_company: "Home Respiratory Therapy and Equipment, Inc.",
    diagnosis_code: "V8672XA",
    email: "kcriag10@csmonitor.com",
    drug_name: "Oxygen",
  },
  {
    id: 38,
    drug_company: "Western Family Foods Inc",
    diagnosis_code: "M898X4",
    email: "pmeineck11@wsj.com",
    drug_name: "Guaifenesin",
  },
  {
    id: 39,
    drug_company:
      "Wyeth Pharmaceutical Division of Wyeth Holdings Corporation, a subsidiary of Pfizer Inc.",
    diagnosis_code: "S92513S",
    email: "jstirton12@delicious.com",
    drug_name: "Pneumococcal 7-valent",
  },
  {
    id: 40,
    drug_company: "PD-Rx Pharmaceuticals, Inc.",
    diagnosis_code: "A288",
    email: "cglasser13@abc.net.au",
    drug_name: "lansoprazole",
  },
  {
    id: 41,
    drug_company: "ReadyMeds",
    diagnosis_code: "V940",
    email: "ubaudy14@linkedin.com",
    drug_name: "Atenolol",
  },
  {
    id: 42,
    drug_company: "NATURE REPUBLIC CO., LTD.",
    diagnosis_code: "F1515",
    email: "ftabourel15@yellowpages.com",
    drug_name: "WITCH HAZEL",
  },
  {
    id: 43,
    drug_company: "Ningbo Dowland Daily Health Co., Ltd",
    diagnosis_code: "G122",
    email: "swernher16@github.com",
    drug_name: "ALCOHOL",
  },
  {
    id: 44,
    drug_company: "Dynamic Pharmaceuticals Inc.",
    diagnosis_code: "B75",
    email: "mrubinowitz17@constantcontact.com",
    drug_name: "Zinc Acetate and Zinc Gluconate",
  },
  {
    id: 45,
    drug_company: "INDICUS PHARMA LLC",
    diagnosis_code: "I70262",
    email: "jrispine18@addthis.com",
    drug_name: "DONEPEZIL",
  },
  {
    id: 46,
    drug_company: "Unifirst First Aid Corporation",
    diagnosis_code: "T3996XS",
    email: "lperrin19@webeden.co.uk",
    drug_name: "bacitracin zinc, neomycin sulfate, polymyxin b sulfate",
  },
  {
    id: 47,
    drug_company: "Rite Aid",
    diagnosis_code: "T471X",
    email: "erobardey1a@bravesites.com",
    drug_name: "Acetaminophen, Aspirin and caffeine",
  },
  {
    id: 48,
    drug_company: "Mylan Institutional Inc.",
    diagnosis_code: "X378XXS",
    email: "bbauman1b@usatoday.com",
    drug_name: "escitalopram",
  },
  {
    id: 49,
    drug_company: "Ranbaxy Laboratories Inc.",
    diagnosis_code: "S93529A",
    email: "dbackhurst1c@gizmodo.com",
    drug_name: "minocycline hydrochloride",
  },
  {
    id: 50,
    drug_company: "Nelco Laboratories, Inc.",
    diagnosis_code: "M12831",
    email: "vwoodcock1d@utexas.edu",
    drug_name: "Wheat Smut",
  },
  {
    id: 51,
    drug_company: "Western Family Foods, Inc.",
    diagnosis_code: "S169XXD",
    email: "lmarcham1e@deliciousdays.com",
    drug_name: "Phenol",
  },
  {
    id: 52,
    drug_company: "NASH-FINCH COMPANY",
    diagnosis_code: "S90812",
    email: "agrebert1f@deviantart.com",
    drug_name: "Diphenhydramine HCl",
  },
  {
    id: 53,
    drug_company: "GRIFOLS USA, LLC",
    diagnosis_code: "S023",
    email: "fquartermain1g@army.mil",
    drug_name: "Albumin (Human)",
  },
  {
    id: 54,
    drug_company: "Major Pharmaceuticals",
    diagnosis_code: "S93305",
    email: "dcanizares1h@theglobeandmail.com",
    drug_name: "Levofloxacin",
  },
  {
    id: 55,
    drug_company: "APOTHECA, INC",
    diagnosis_code: "V4333XD",
    email: "ngerber1i@microsoft.com",
    drug_name: "TOPIRAMATE",
  },
  {
    id: 56,
    drug_company: "Eisai Inc.",
    diagnosis_code: "M80821P",
    email: "lcrisford1j@usa.gov",
    drug_name: "dalteparin sodium",
  },
  {
    id: 57,
    drug_company: "Lupin Pharmaceuticals, Inc.",
    diagnosis_code: "T84129S",
    email: "jgrenter1k@wordpress.com",
    drug_name: "NIACIN",
  },
  {
    id: 58,
    drug_company: "Mist Pharmaceuticals, LLC",
    diagnosis_code: "E329",
    email: "cmeehan1l@time.com",
    drug_name: "propranolol hydrochloride",
  },
  {
    id: 59,
    drug_company: "sanofi-aventis U.S. LLC",
    diagnosis_code: "T50A93",
    email: "zhedditch1m@shinystat.com",
    drug_name: "dolasetron mesylate",
  },
  {
    id: 60,
    drug_company: "RESOLUTIONS LLC",
    diagnosis_code: "S81802A",
    email: "hcochet1n@hubpages.com",
    drug_name: "ZINC OXIDE, EUCALYPTUS OIL",
  },
  {
    id: 61,
    drug_company: "Wockhardt Limited",
    diagnosis_code: "M435X5",
    email: "tgentry1o@umich.edu",
    drug_name: "Midazolam hydrochloride",
  },
  {
    id: 62,
    drug_company: "Actavis Pharma, Inc.",
    diagnosis_code: "S92326P",
    email: "gmcauslan1p@ow.ly",
    drug_name: "Hydrocodone Bitartate and Acetaminophen",
  },
  {
    id: 63,
    drug_company: "Western Family Foods Inc",
    diagnosis_code: "S82301G",
    email: "gavis1q@webs.com",
    drug_name: "Dextromethorphan HBr, Guaifenesin",
  },
  {
    id: 64,
    drug_company: "Merck Sharp & Dohme Corp.",
    diagnosis_code: "O3433",
    email: "scastellone1r@gravatar.com",
    drug_name: "FOSAPREPITANT DIMEGLUMINE",
  },
  {
    id: 65,
    drug_company: "Physicians Total Care, Inc.",
    diagnosis_code: "S82256J",
    email: "jscocroft1s@sfgate.com",
    drug_name: "Lithium Carbonate",
  },
  {
    id: 66,
    drug_company: "CorePharma, LLC",
    diagnosis_code: "M418",
    email: "etandy1t@booking.com",
    drug_name: "Pyridostigmine Bromide",
  },
  {
    id: 67,
    drug_company: "Natural Health Supply",
    diagnosis_code: "S0450XD",
    email: "tcantillon1u@creativecommons.org",
    drug_name: "HELIANTHUS ANNUUS FLOWER",
  },
  {
    id: 68,
    drug_company: "L Perrigo Company",
    diagnosis_code: "I70428",
    email: "tpritchett1v@vinaora.com",
    drug_name: "acetaminophen,dextromethorphan HBr, doxylamine succinate",
  },
  {
    id: 69,
    drug_company: "AMOREPACIFIC",
    diagnosis_code: "S52569G",
    email: "aguest1w@paginegialle.it",
    drug_name: "Octinoxate, TITANIUM DIOXIDE, and ZINC OXIDE",
  },
  {
    id: 70,
    drug_company: "Capital Pharmaceutical, LLC",
    diagnosis_code: "M8781",
    email: "craeburn1x@istockphoto.com",
    drug_name: "Dextromethorphan HBr and Pyrilamine Maleate",
  },
  {
    id: 71,
    drug_company: "SHISEIDO AMERICA INC.",
    diagnosis_code: "S3217XK",
    email: "sralphs1y@indiatimes.com",
    drug_name: "Octinoxate and Octocrylene",
  },
  {
    id: 72,
    drug_company: "Teva Women's Health, Inc.",
    diagnosis_code: "T63891D",
    email: "psackey1z@topsy.com",
    drug_name: "Norethindrone Acetate/Ethinyl Estradiol and Ferrous Fumarate",
  },
  {
    id: 73,
    drug_company: "CVS",
    diagnosis_code: "T8325XA",
    email: "aarlow20@nih.gov",
    drug_name:
      "Doxylamine Succinate, Acetaminophen, Dextromethorphan Hydrobromide",
  },
  {
    id: 74,
    drug_company: "La Prairie, Inc.",
    diagnosis_code: "S062X4A",
    email: "jgumly21@psu.edu",
    drug_name: "OCTINOXATE",
  },
  {
    id: 75,
    drug_company: "Deseret Biologicals, Inc.",
    diagnosis_code: "S56196A",
    email: "sattwater22@stanford.edu",
    drug_name: "not applicable",
  },
  {
    id: 76,
    drug_company: "Bryant Ranch Prepack",
    diagnosis_code: "Z8501",
    email: "prichin23@fc2.com",
    drug_name: "Chlordiazepoxide Hydrochloride",
  },
  {
    id: 77,
    drug_company: "NCS HealthCare of KY, Inc dba Vangard Labs",
    diagnosis_code: "S60822D",
    email: "dcomben24@imageshack.us",
    drug_name: "ENALAPRIL MALEATE",
  },
  {
    id: 78,
    drug_company: "Colgate-Palmolive Company",
    diagnosis_code: "V9121XD",
    email: "lrighy25@sciencedaily.com",
    drug_name: "SODIUM FLUORIDE and TRICLOSAN",
  },
  {
    id: 79,
    drug_company: "Cadila Pharmaceuticals Limited",
    diagnosis_code: "T378X6D",
    email: "dvanini26@w3.org",
    drug_name: "hydralazine hydrochloride",
  },
  {
    id: 80,
    drug_company: "Access Busines Group LLC",
    diagnosis_code: "S82046G",
    email: "rpiddle27@icio.us",
    drug_name: "ZINC OXIDE, OCTINOXATE",
  },
  {
    id: 81,
    drug_company: "Physicians Total Care, Inc.",
    diagnosis_code: "M5090",
    email: "csnodden28@paypal.com",
    drug_name: "calcitriol",
  },
  {
    id: 82,
    drug_company: "Phillips Company",
    diagnosis_code: "S42431D",
    email: "jbroadbere29@blogtalkradio.com",
    drug_name: "SALICYLIC ACID",
  },
  {
    id: 83,
    drug_company: "Marlex Pharmaceuticals Inc",
    diagnosis_code: "O628",
    email: "bhowlett2a@discuz.net",
    drug_name: "Probenecid",
  },
  {
    id: 84,
    drug_company: "Dynarex Corporation",
    diagnosis_code: "X05XXXD",
    email: "tdegoe2b@jimdo.com",
    drug_name: "zinc oxide",
  },
  {
    id: 85,
    drug_company: "DermaRite Industries, LLC",
    diagnosis_code: "N159",
    email: "ypena2c@elegantthemes.com",
    drug_name: "OTC SKIN PROTECTANT DRUG PRODUCT",
  },
  {
    id: 86,
    drug_company: "AGP LLC",
    diagnosis_code: "Y367X",
    email: "asnowsill2d@economist.com",
    drug_name: "NITROGEN",
  },
  {
    id: 87,
    drug_company: "Mission Hills, S.A. de C.V.",
    diagnosis_code: "S198",
    email: "kbickell2e@blogs.com",
    drug_name: "SODIUM FLUORIDE",
  },
  {
    id: 88,
    drug_company: "Supervalu Inc",
    diagnosis_code: "S14153S",
    email: "ckeynd2f@fema.gov",
    drug_name: "Acetaminophen, Dextromethorphan HBr, Doxylamine succinate",
  },
  {
    id: 89,
    drug_company: "Jubilant HollisterStier LLC",
    diagnosis_code: "Y9201",
    email: "achaffer2g@businesswire.com",
    drug_name: "White Faced Hornet hymenoptera venom Venomil Maintenance",
  },
  {
    id: 90,
    drug_company: "Parfums Christian Dior",
    diagnosis_code: "T23669",
    email: "hingall2h@howstuffworks.com",
    drug_name: "OCTINOXATE, AVOBENZONE",
  },
  {
    id: 91,
    drug_company: "Lake Erie Medical DBA Quality Care Products LLC",
    diagnosis_code: "S42015",
    email: "iianne2i@shop-pro.jp",
    drug_name: "Paroxetine",
  },
  {
    id: 92,
    drug_company: "Sandoz Inc",
    diagnosis_code: "S42018S",
    email: "kavieson2j@deliciousdays.com",
    drug_name: "Topotecan",
  },
  {
    id: 93,
    drug_company: "Native Remedies, LLC",
    diagnosis_code: "S06351D",
    email: "astirzaker2k@eepurl.com",
    drug_name: "Angelica, Arnica, Mag phos, Passiflora, Silicea",
  },
  {
    id: 94,
    drug_company: "Onset Dermatologics LLC",
    diagnosis_code: "S82209A",
    email: "tsibray2l@networkadvertising.org",
    drug_name: "TRETINOIN",
  },
  {
    id: 95,
    drug_company: "Nelco Laboratories, Inc.",
    diagnosis_code: "S72414J",
    email: "gmcinerney2m@yahoo.co.jp",
    drug_name: "Silk",
  },
  {
    id: 96,
    drug_company: "Taro Pharmaceuticals U.S.A. Inc.",
    diagnosis_code: "L8962",
    email: "gbaynham2n@reference.com",
    drug_name: "Acetaminophen",
  },
  {
    id: 97,
    drug_company: "Colgate-Palmolive Canada",
    diagnosis_code: "S72355Q",
    email: "lgrubey2o@yellowpages.com",
    drug_name: "Sodium Fluoride",
  },
  {
    id: 98,
    drug_company: "Camber Pharmaceuticals, Inc.",
    diagnosis_code: "E75249",
    email: "kclowney2p@blog.com",
    drug_name: "Finasteride",
  },
  {
    id: 99,
    drug_company: "Unit Dose Services",
    diagnosis_code: "M532",
    email: "wgowthorpe2q@eepurl.com",
    drug_name: "Allopurinol",
  },
  {
    id: 100,
    drug_company: "Teva Pharmaceuticals USA Inc",
    diagnosis_code: "S86109",
    email: "mshreve2r@miitbeian.gov.cn",
    drug_name: "Amlodipine Besylate and Benazepril Hydrochloride",
  },
];
