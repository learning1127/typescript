import SelectData from './components/SelectData';
import { Employer, employers } from './data/employers';
import { Car, cars } from './data/cars';
import { Drug, drugs } from './data/drugs';
import searchGeneric from './util/search';
import { useState } from 'react';
import QueryInput from './components/QueryInput';
import ItemsFilter from './components/ItemsFilter';
import './App.css';
import {
  createObjectForCheckboxFilter,
  propertiesKeys,
} from './util/createObjectForChecboxFilter';

export interface CollectionData {
  employers: Employer[];
  cars: Car[];
  drugs: Drug[];
}

export type KeyOfCollectionData = keyof CollectionData;

// TODO: implements correctly GetProperties from CollectionData
// type mergeDataInterface = propertiesKeys<ThisType<CollectionData>>;
type OrTypeCollections = propertiesKeys<Employer | Car | Drug>;

const provider: CollectionData = {
  employers: employers,
  cars: cars,
  drugs: drugs,
};

function App() {
  const [query, setQuery] = useState<string>('');
  const [objectForProperties, setObjectForProperties] =
    useState<OrTypeCollections>(
      createObjectForCheckboxFilter({ objectModel: cars[0] })
    );
  const [searchBy, setSearchBy] = useState<Array<keyof OrTypeCollections>>([]);
  const [dataView, setDataView] = useState<Array<OrTypeCollections>>(cars);

  const handleChangeData = (selectedData: KeyOfCollectionData) => {
    const selectedDataArray = provider[selectedData];
    setObjectForProperties(
      createObjectForCheckboxFilter({ objectModel: selectedDataArray[0] })
    );

    setDataView(selectedDataArray);
  };

  return (
    <div className="App">
      <div className="query-select-box">
        <SelectData allData={provider} selectData={handleChangeData} />
        <QueryInput query={query} setQuery={setQuery} />
      </div>
      <ItemsFilter
        objectForProperties={objectForProperties}
        setObjectForProperties={setObjectForProperties}
        setSearchBy={setSearchBy}
      />

      {dataView
        .filter((item) => searchGeneric(item, searchBy, query))
        .map((item) => {
          return (
            <pre key={item.id}>
              <code>{JSON.stringify(item, null, 2)}</code>
            </pre>
          );
        })}
    </div>
  );
}

export default App;
