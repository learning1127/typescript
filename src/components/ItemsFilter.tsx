import React, { Dispatch } from "react";
import { EmployersField, ObjectProperties } from "../@types/Employer";
import { propertiesKeys } from "../util/createObjectForChecboxFilter";

interface Props<T> {
  objectForProperties: propertiesKeys<T>;
  setObjectForProperties: Dispatch<React.SetStateAction<propertiesKeys<T>>>;
  setSearchBy: Dispatch<React.SetStateAction<Array<keyof T>>>;
}

const ItemsFilter = <T extends unknown>({
  objectForProperties,
  setObjectForProperties,
  setSearchBy,
}: Props<T>) => {
  return (
    <div className="group_filter">
      {Object.keys(objectForProperties).map((property) => {
        return (
          <div className="group_filter__item" key={`filter-by-${property}`}>
            <input
              type="checkbox"
              name={property}
              value={property}
              onChange={({ target }) => {
                const objectIterable = objectForProperties[property as keyof T];
                target.checked
                  ? setSearchBy((prevSearch) => {
                      return [...prevSearch, target.name as keyof T];
                    })
                  : setSearchBy((prevSearch) => {
                      return prevSearch.filter(
                        (search) => search !== target.name
                      );
                    });

                setObjectForProperties((prevState) => {
                  return {
                    ...prevState,
                    [target.name]: {
                      ...objectIterable,
                      isChecked: target.checked,
                    },
                  };
                });
              }}
            />
            <label htmlFor={property}>{property.replaceAll("_", " ")}</label>
          </div>
        );
      })}
    </div>
  );
};

export default ItemsFilter;
