import React, { Dispatch } from "react";

interface Props {
  query: string;
  setQuery: Dispatch<React.SetStateAction<string>>;
}

const QueryInput: React.FC<Props> = ({ query, setQuery }) => {
  return (
    <input
      className="query"
      type="text"
      value={query}
      onChange={({ target }) => {
        setQuery(target.value);
      }}
    />
  );
};

export default QueryInput;
