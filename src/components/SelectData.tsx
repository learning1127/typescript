import { CollectionData, KeyOfCollectionData } from "../App";

interface Props {
  allData: CollectionData;
  selectData: (dataSelected: KeyOfCollectionData) => void;
}

const SelectData = ({ allData, selectData }: Props) => {
  return (
    <select
      className="select-data"
      onChange={({ target }) => {
        selectData(target.value as KeyOfCollectionData);
      }}
    >
      {Object.keys(allData).map((data) => {
        return (
          <option key={data} value={data}>
            {data}
          </option>
        );
      })}
    </select>
  );
};

export default SelectData;
