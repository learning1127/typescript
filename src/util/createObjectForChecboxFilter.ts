export interface objectForChecbox<U> {
  objectModel: U;
}

export interface structureCheckbox {
  isChecked: boolean;
  name: string;
}

export type propertiesKeys<V> = Record<keyof V, structureCheckbox>;

export const createObjectForCheckboxFilter = <T extends {}>({
  objectModel,
}: objectForChecbox<T>): propertiesKeys<T> => {
  const arrayKeys = Object.keys(objectModel) as Array<keyof T>;
  const createObjectState: any = {};
  arrayKeys.forEach((key) => {
    Object.assign(createObjectState, {
      [key]: { name: key, isChecked: false },
    });
  });

  return createObjectState;
};
