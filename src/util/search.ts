const search = <T>(object: T, properties: Array<keyof T>, query: string) => {
  return (
    query !== "" &&
    properties.some((property) => {
      const valueToCompare = object[property];
      return (
        (typeof valueToCompare === "string" ||
          typeof valueToCompare === "number") &&
        valueToCompare
          .toString()
          .toLocaleLowerCase()
          .includes(query.toLocaleLowerCase())
      );
    })
  );
};

export default search;
