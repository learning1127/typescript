const sort = <T>(prev: T, compare: T, property: keyof T) => {
  if (prev[property] > compare[property]) {
    return 1;
  } else {
    if (prev[property] < compare[property]) {
      return -1;
    } else {
      return 0;
    }
  }
};

export default sort;
