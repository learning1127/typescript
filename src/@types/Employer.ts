import { Employer } from "../data/employers";

export type EmployersField = keyof Employer;
export type ObjectProperties = Record<
  EmployersField,
  { name: EmployersField; isChecked: boolean }
>;
